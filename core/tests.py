from django.test import TestCase
from django.urls import reverse
from .models import Service
from store.models import Product
from .views import frontpage


class FrontpageViewTest(TestCase):

    def setUp(self):
        # Create some sample data for testing
        self.product1 = Product.objects.create(name="Product 1", price=10)
        self.product2 = Product.objects.create(name="Product 2", price=20)
        self.service1 = Service.objects.create(name="Service 1", status=Service.ACTIVE)
        self.service2 = Service.objects.create(name="Service 2", status=Service.ACTIVE)

    def test_frontpage_view(self):
        # Make a GET request to the frontpage view
        response = self.client.get(reverse('frontpage'))

        # Check that the response status code is 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Check that the correct template is being used
        self.assertTemplateUsed(response, 'frontpage.html')

        # Check that the 'services' context contains active services
        self.assertTrue('services' in response.context)
        services = response.context['services']
        self.assertEqual(len(services), 2)  # Assuming there are 2 active services

        # Check that each service has an average rating attribute
        for service in services:
            self.assertTrue(hasattr(service, 'avg_rating'))

        # Check that the 'products' context contains the correct products
        self.assertTrue('products' in response.context)
        products = response.context['products']
        self.assertEqual(len(products), 2)  # Assuming there are 2 products displayed on the frontpage