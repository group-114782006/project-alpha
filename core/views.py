from django.shortcuts import render, get_object_or_404
from storefront.models import Service


def frontpage(request):
    services = Service.objects.filter(status=Service.ACTIVE)

    for service in services:
        service.avg_rating = service.average_rating()

    context = {
        'services': services
    }

    return render(request, 'frontpage.html', {
        'services': services
    })


def about(request):
    return render(request, 'about.html')


def custom_404(request, exception):
    return render(request, '404.html', status=404)

