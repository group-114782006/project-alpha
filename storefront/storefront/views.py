from django.db.models import Avg, Q
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
import requests
from .models import Category, Service, Review, Appointment
from .forms import ReviewForm, AppointmentForm
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy


#@login_required(login_url=reverse_lazy('userprofile:login'))
def make_appointment(request, service_id):
    service = Service.objects.get(pk=service_id)
    context = {
        'service': service,
        'service_title': service.title  # Assuming the title field name is 'title'
    }
    return render(request, 'appointment.html', context)


def search(request):
    query = request.GET.get('query', '')
    services = Service.objects.filter(status=Service.ACTIVE).filter(
        Q(title__icontains=query) | Q(description__icontains=query)
    )

    for service in services:
        service.avg_rating = service.average_rating()

    context = {
        'services': services
    }

    return render(request, 'search.html', {
        'query': query,
        'services': services
    })


def category_detail(request, slug):
    category = get_object_or_404(Category, slug=slug)
    services = category.services.filter(status=Service.ACTIVE)

    return render(request, 'category_detail.html', {
        'category': category,
        'services': services,
    })
    

def service_detail(request, category_slug, slug):
    service = get_object_or_404(Service, slug=slug, status=Service.ACTIVE)
    reviews = Review.objects.filter(service=service)
    avg_rating = reviews.aggregate(avg_rating=Avg('rating'))['avg_rating']

    user_review = None
    is_vendor = False  # Default value for is_vendor

    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.service = service
            review.user = request.user
            review.save()
            return redirect('service_detail', category_slug=category_slug, slug=slug)
    else:
        form = ReviewForm()

    # Check if the user is authenticated and adjust variables accordingly
    if request.user.is_authenticated:
        user_review = Review.objects.filter(service=service, user=request.user).first()
        
    # Check if the current user is a vendor
    #is_vendor = request.user.services.filter(id=service.id).exists()

    return render(request, 'service_detail.html', {
        'service': service,
        'reviews': reviews,
        'avg_rating': avg_rating,
        'form': form,
        'user_review': user_review,
        
    })


def managepage(request):
    if request.user.is_authenticated:
        current_user = request.user
        my_services = Service.objects.filter(user=current_user)
        print(my_services)
        return render(request, 'managepage.html', {'services': my_services} )
    

def manage(request, service_id):
    # current_user = request.user
    my_service = get_object_or_404(Service, id=service_id)
    my_appointment = Appointment.objects.filter(service=my_service)
    print(my_service)
    print(my_appointment)
    return render(request, 'manage.html', {"services":my_service, "appointments":my_appointment}) 

def approve(request, appointment_id,service_id ):
    appointment = get_object_or_404(Appointment, id = appointment_id)
    appointment.status = "Accepted"
    appointment.save()
    return redirect('manage' , service_id = service_id)


def deny(request, appointment_id, service_id):
    appointment = get_object_or_404(Appointment, id = appointment_id)
    appointment.status = "Denied"
    appointment.save()
    return redirect('manage' , service_id = service_id)


def apply(request, service_id):
    form = AppointmentForm()
    service = get_object_or_404(Service, id = service_id)
    
    if request.user.is_authenticated:
        customer = request.user
        if request.method == "POST":
            appointment_time = request.POST.get("appointment_time")            
            Appointment.objects.create(
                service = service,
                customer = customer,
                time = appointment_time ,

                  )
            return redirect('frontpage')
    return render(request, 'appointment.html', {'form': form, "service":service})


def view_appointment_status(request):
    user = request.user
    my_appointments = Appointment.objects.filter(customer=user).order_by('-created_at')
    return render(request, "status.html", {"appointments": my_appointments} )