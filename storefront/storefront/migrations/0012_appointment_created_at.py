# Generated by Django 5.0.1 on 2024-04-27 13:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storefront', '0011_appointment_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
