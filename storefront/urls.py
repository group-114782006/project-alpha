from django.urls import path

from . import views

urlpatterns = [
    path('search/', views.search, name = 'search'),
    path('make_appointment/<int:service_id>/', views.make_appointment, name='make_appointment'),
    path('category_detail/<slug:slug>/', views.category_detail, name='category_detail'),
    path('service_detail/<slug:category_slug>/<slug:slug>/', views.service_detail, name='service_detail'),
    path("managepage/", views.managepage, name='managepage'),
    path("manage/<int:service_id>/", views.manage, name='manage'),
    path("approve/<int:appointment_id>/<int:service_id>/", views.approve , name="approve"),
    path("deny/<int:appointment_id>/<int:service_id>/", views.deny , name="deny"),
    path('apply/<int:service_id>/', views.apply, name='apply'),    
    path('view_status/', views.view_appointment_status, name='status')
    

]