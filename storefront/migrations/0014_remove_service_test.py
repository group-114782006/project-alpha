# Generated by Django 5.0.3 on 2024-05-16 10:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('storefront', '0013_service_test'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='service',
            name='test',
        ),
    ]
