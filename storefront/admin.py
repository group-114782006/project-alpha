from django.contrib import admin

from .models import Category, Service, District, Appointment

admin.site.register(Category)
admin.site.register(Service)
admin.site.register(District)
admin.site.register(Appointment)