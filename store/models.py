from django.contrib.auth.models import User
from django.db import models

from statistics import mode
from django.contrib.auth.models import User
from django.db import models
from django.utils.text import slugify
from django.db.models import Avg
from django.core.files import File
from django.core.validators import MinValueValidator, MaxValueValidator

from io import BytesIO
from PIL import Image




# Create your models here.
class ProductCategories(models.Model):
    title = models.CharField(max_length = 50)
    slug =models.SlugField(max_length = 50)

class Meta:
    verbose_name_plural =' Product Categories'

def __str__(self):
        return self.title

class Product(models.Model):
    user = models.ForeignKey(User, related_name='products', on_delete=models.CASCADE)
    category = models.ForeignKey(ProductCategories, related_name='products', on_delete=models.CASCADE)
    title = models.CharField(max_length = 50)
    slug =models.SlugField(max_length = 50)
    description = models.TextField(blank=True)
    price = models.IntegerField()
    rating = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(5)])
    comment = models.TextField()